一覧画面<br>
<?php echo Html::anchor('vote/logout', 'ログアウト') ?><br>
<div class="container">
  <ul class="row">
  	<?php foreach ($images as $img):?>
  		<li class="col-md-4">
  			<?php echo Asset::img($img['file_name']); ?><br>
  			<span class="votes"><?php echo $img['votes'] ?></span><br>
        		<input id="voteBtn" type="button" name="vote" class="vote" data-id="<?php echo $img['id']?>" value="投票する">
  		</li>
  	<?php endforeach; ?>
  </ul>
</div>

<script>
  $( function () {
    var $voteBtn = $('input.vote');

    $voteBtn.on( 'click', function (event) {
      var id = $(this).data('id');
      $.ajax({
        url: "<?php echo Uri::create('api/vote.json') ?>",
        type: 'post',
        data: {id: id},
        dataType: 'json'
      }).then( function (data) {
        console.log(data);
        location.reload();
      }, function (error) {
        console.log(error);
      });
    });
  });
</script>
