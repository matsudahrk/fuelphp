<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8" >
  <title><?php echo isset($title) ? $title : 'Document'; ?></title>
  <?php echo Asset::css('normalize.css') ?>
  <?php echo Asset::css('bootstrap.css') ?>
  <script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
  <style media="screen">
    img {
      max-width: 100%;
      vertical-align: bottom;
    }
    ul {
      padding: none;
      list-style: none;
    }
  </style>
</head>
<body>
    <header>
        <!-- header.phpファイルを読み込む-->
        <?php echo $header; ?>
    </header>
    <div id="content">
        <!-- 各アクションの内容を読み込む-->
        <?php echo $content; ?>
    </div>
    <footer>
        <!-- footer.phpファイルを読み込む-->
        <?php echo $footer; ?>
    </footer>
</body>
</html>
