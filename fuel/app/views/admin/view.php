
一覧画面<br>
<?php echo Html::anchor('admin/logout', 'ログアウト') ?><br>
<?php echo Html::anchor('admin/upload', 'アップロード') ?>

<ul class="row">
	<?php foreach ($images as $img):?>
		<li class="col-md-4">
			<?php echo Asset::img($img['file_name']); ?><br>
			<span class="votes"><?php echo $img['votes'] ?></span>
		</li>
	<?php endforeach; ?>
</ul>
