const express     = require('express');
const compression = require('compression');
const logger      = require('morgan');
const router      = express.Router();
const bodyParser  = require('body-parser');
const server      = express();
const fs          = require('fs');
const path        = require('path');

const electron = require('electron');
const remote   = electron.remote;
const Menu     = electron.Menu;
const MenuItem = electron.MenuItemu;

server.use(express.static(path.join(__dirname, 'public')));
server.listen(3000, "127.0.0.1");

server.use(compression({
  threshold: 0,
  level:     9,
  memLevel:  9
}));

//server.use( (req, res, next) => {
//  res.header('Access-Control-Allow-Origin', '*');
//  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//  next();
//});

//server.use(bodyParser());
server.use(bodyParser.urlencoded({ extended: true }));

server.use(bodyParser.json());

server.use(express.static('public'));

server.use(logger('dev'));

// POSTの場合はreq.body

const PlayRoute   = require('./routes/play.js');
const NativeRoute = require('./routes/native.js');
const SaveRoute   = require('./routes/save.js');
const GetRoute    = require('./routes/get.js');
const list    = require('./routes/list.js');

server.use('/play', PlayRoute);
server.use('/native', NativeRoute);
server.use('/save', SaveRoute);
server.use('/get', GetRoute);
server.use('/list', list);

server.use('/', router.get('/', (req, res, next) => {
  res.render('index.html');
  res.end();
}));

module.exports = server;
