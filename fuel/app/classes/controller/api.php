<?php


class Controller_Api extends Controller_Rest
{
  public function post_vote()
  {
    if ( !Auth::check() ) {
      return $this->response(
        array('message' => 'ログインしていません。')
      );
    }
    $id = Input::post('id');

    $img = Model_Image::find($id);
    $img->votes = $img->votes + 1;
    $img->save();
    $username = Auth::get_screen_name();
    return $this->response(
      array(
        'message' => '処理終了',
        'image' => $img,
        'username' => $username,
        )
    );

  }
}
