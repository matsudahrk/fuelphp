<?php

class Controller_Admin extends Controller_Base
{
	public function action_login()
	{
		if ( Input::post('id') === 'admin'
					&& Input::post('password') == 'pass') {
			Session::set('admin_login', true);
		}
		if ( Session::get('admin_login') === true ) {
			return Response::redirect('admin/view');
		}
		$data = array(
			'message' => '管理者向けログイン機能です。',
		);
    $this->template->title = '管理者ログイン';
		$this->template->content = View::forge('admin/login', $data);
	}

	public function action_view()
	{
		self::confirm_login();

		$images = Model_Image::find('all');
		$data = array('images' => $images);
    $this->template->title = '管理者用画像一覧';
		$this->template->content = View::forge('admin/view', $data);
	}

	public function action_upload()
	{

		self::confirm_login();

		if (Input::file('image')) {
			$image = Input::file('image');
			move_uploaded_file($image['tmp_name'], 'assets/img/'. $image['name']);

			$model_image = new Model_Image(); // 新しいレコードを作る
			$model_image->file_name = $image['name'];
			$model_image->info = '';
			$model_image->votes = 0;
			$model_image->save();
		}
    $this->template->title = '画像アップロード画面';
		$this->template->content = View::forge('admin/upload');
	}

	public function action_logout()
	{
		Session::delete('admin_login');
		Response::redirect('admin/login');
	}

	private function confirm_login() {
		if ( !Session::get('admin_login') ) {
			Response::redirect('admin/login');
		}
	}

}
