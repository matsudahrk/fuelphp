<?php

class Controller_Vote extends Controller_Base
{
	public function action_login()
	{
		if (Input::post('username') !== '') {
			if ( Auth::login(Input::post('username'), Input::post('password')) )
			{
				Response::redirect('vote/view');
			}
		}
		$data = array(
			'title' => 'foo',
		);
    $this->template->title = 'ユーザーログイン画面';
		$this->template->content = View::forge('vote/login', $data);
	}
	public function action_logout()
	{
		Auth::logout();
		Response::redirect('vote/login');
	}
	public function action_view()
	{
		if ( !Auth::check() ) {
			Response::redirect('vote/login');
		}
		$images = Model_Image::find('all');
		$data = array(
      'images' => $images
    );
    $this->template->title = '一覧画面';
		$this->template->content = View::forge('vote/view', $data);
	}
}
